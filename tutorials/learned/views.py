import json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views import generic
from .models import Content, Step, Paragraph
from .forms import UploadFileForm
from .handlers import handle_uploaded_file



def index(request):
    return HttpResponse("<h1>Learned Index</h1>")

class IndexView(generic.ListView):
    template_name = 'learned/search.html'
    context_object_name = 'contents'

    def get_queryset(self):
        return Content.objects.order_by('-heading')

class ContentDetailView(generic.DetailView):
    model = Content
    template = 'learned/detailed.html'
    context_object_name = 'content'


def search(request):
    search = request.GET['query']
    contents = Content.objects.filter(heading__icontains=search)
    return render(request, 'learned/search.html', {
        'contents' : contents,
        'search' : search
        })

def all(request):
    response = Content.objects.all()

    return JsonResponse( { 'response' : response } )


def get_step(pk):
    return Step.objects.filter(content=pk)

def get_paragraph(pk):
    return Paragraph.objects.filter(step=pk)

#def detail(request, pk):
#    content = Content.objects.get(id=pk)
#    steps = get_step(pk)
#    i = 0
#    for step in steps:
#        paragraphs = get_paragraph(step.id)
#        steps[i].paragraphs = paragraphs
#        i += 1
#    content.steps = steps
#
#    return render(request, 'learned/detailed.html', { 'content' : content })

def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)

        if form.is_valid():
            identifier = request.POST['identifier']
            handle_uploaded_file(request.FILES['file'], identifier)
            load_uploaded_file('/tmp/' + identifier + '.json')
            return HttpResponseRedirect('/')
    else:
        form = UploadFileForm()

    return render(request, 'learned/upload.html', {'form': form })

def insert_content_json(data):

    heading = data["content"][0]["heading"]
    print(heading)
    subheading = data["content"][0]["subheading"]
    print(subheading)
    footer = "Django"
    print(footer)
    content = Content(heading=heading, subheading=subheading, footer=footer)
    content.save()
    print("ID : " + str(content.id) )
    steps = data["content"][0]["steps"]
    insert_step_json(steps, content)

def insert_step_json(steps,content):

    for step in steps:
        step_number = step["step_number"]
        step_name = step["step_name"]
        step_obj = Step(step_number = step_number, step_name = step_name, content = content)
        step_obj.save()
        insert_paragraph_json(step["paragraphs"], step_obj)

def insert_paragraph_json(paragraphs, step):
    for paragraph in paragraphs:
        head = paragraph["head"]
        order = paragraph["order"]
        text = paragraph["text"]
        text_type = paragraph["type"]

        paragraph_obj = Paragraph(head = head, order = order, text = text, text_type = text_type, step = step)
        paragraph_obj.save()

def load_uploaded_file(file):

    with open(file, 'r') as read_file:
        data = json.load(read_file)
    data = json.dumps(data)
    data = json.loads(data)
    insert_content_json(data)
