import json
from django.conf import settings



def handle_uploaded_file(f, title):
    with open(settings.MEDIA_ROOT + title +'.json', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def insert_file(file):
    with open(file, 'r') as read_file:
        data = json.load(read_file)
        content = json.dumps(data)
        print(content)
